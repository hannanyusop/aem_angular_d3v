import { Component } from '@angular/core';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss'],
  inputs: [
    'title',
    'subtitle',
  ],
})
export class PanelComponent {

  title : string = '';
  subtitle : string = '';

}
