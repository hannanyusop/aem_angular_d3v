import {UserResponseModel} from "./user-response.model";
import {ChartBarResponseModel} from "./chart-bar-response.model";
import {ChartDonutResponseModel} from "./chart-donut-response.model";


export interface DashboardResponseModel {
  tableUsers: UserResponseModel[];
  chartBar: ChartBarResponseModel[];
  chartDonut: ChartDonutResponseModel[];
  success: boolean;
}
